<?php

require_once('./fpdf181/fpdf.php');

/* Valores recebidos do formulário  */

$nome = $_POST['nome'];
$cpf = $_POST['cpf'];
$cep = $_POST['cep'];
$rua = $_POST['rua'];
$bairro = $_POST['bairro'];
$cidade = $_POST['cidade'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$nomeEmpresa = $_POST['nomeEmpresa'];
$cargoEmpresa = $_POST['cargoEmpresa'];
$cidadeEmpresa = $_POST['cidadeEmpresa'];
$telefoneEmpresa = $_POST['telefoneEmpresa'];
$funcoesEmpresa = $_POST['funcoesEmpresa'];
$aboutYou = $_POST['aboutYou'];

$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','B',16);
$pdf->Cell(190,10,'Curriculum',1,0,"C");
$pdf->Ln(10);

$pdf->SetFont('Arial', '', 12);
$pdf->MultiCell(190,10,
"Nome: $nome\n".
"CPF: $cpf\n".
"CEP: $cep\n".
"Rua: $rua\n".
"Bairro: $bairro\n".
"Cidade: $cidade\n".
"Email: $email\n".
"Telefone: $telefone",1);
$pdf->Ln(10);

$pdf->SetFont('Arial','B',16);
$pdf->Cell(190,10,'Ultima empresa',1,0,"C");
$pdf->Ln(10);

$pdf->SetFont('Arial', '', 12);
$pdf->MultiCell(190,10,"Nome: $nomeEmpresa\n".
"Cargo: $cargoEmpresa\n".
"Cidade: $cidadeEmpresa\n".
"Telefone: $telefoneEmpresa\n",1);

$pdf->Output();
